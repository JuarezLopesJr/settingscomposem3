@file:OptIn(ExperimentalComposeUiApi::class, ExperimentalComposeUiApi::class)

package com.example.settingscomposem3.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.PopupProperties
import com.example.settingscomposem3.R
import com.example.settingscomposem3.data.Theme
import com.example.settingscomposem3.utils.Tags.TAG_SELECT_THEME
import com.example.settingscomposem3.utils.Tags.TAG_THEME_ITEM
import com.example.settingscomposem3.utils.Tags.TAG_THEME_OPTION

@Composable
fun ThemeSettings(
    modifier: Modifier = Modifier,
    selectedTheme: Theme,
    onThemeSelected: (Theme) -> Unit
) {
    var expanded by remember { mutableStateOf(false) }

    Row(
        modifier = modifier
            .testTag(TAG_SELECT_THEME)
            .clickable(
                onClickLabel = stringResource(id = R.string.theme_description),
                onClick = { expanded = !expanded }
            )
            .padding(16.dp)
    ) {

        Text(
            modifier = Modifier.weight(1f),
            text = stringResource(id = R.string.theme_option)
        )

        Text(
            modifier = Modifier.testTag(TAG_THEME_ITEM),
            text = stringResource(id = selectedTheme.label)
        )

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            offset = DpOffset(x = 8.dp, y = 4.dp),
            properties = PopupProperties(focusable = true, usePlatformDefaultWidth = true)
        ) {
            Theme.values().forEach { theme ->
                val themeLabel = stringResource(id = theme.label)

                DropdownMenuItem(
                    /* semantics to locate the theme's node options for testing */
                    modifier = Modifier.testTag(TAG_THEME_OPTION + themeLabel),
                    text = { Text(text = stringResource(id = theme.label)) },
                    onClick = {
                        onThemeSelected(theme)
                        expanded = false
                    }
                )
            }
        }
    }
}