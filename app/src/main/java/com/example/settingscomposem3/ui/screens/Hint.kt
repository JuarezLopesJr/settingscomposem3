package com.example.settingscomposem3.ui.screens

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import com.example.settingscomposem3.R
import com.example.settingscomposem3.utils.SettingsToggleableItem
import com.example.settingscomposem3.utils.Tags.TAG_CHECK_ITEM

@Composable
fun HintSettings(
    modifier: Modifier = Modifier,
    title: String,
    checked: Boolean,
    onShowHintToggled: (Boolean) -> Unit
) {
    val hintState = if (checked) {
        stringResource(R.string.hint_enabled_text)
    } else {
        stringResource(R.string.hint_disabled_text)
    }

    SettingsToggleableItem(
        modifier = modifier,
        title = title,
        checked = checked,
        role = Role.Checkbox,
        semanticValue = hintState,
        tag = TAG_CHECK_ITEM,
        onToggled = onShowHintToggled
    )
}