package com.example.settingscomposem3.ui.screens

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp

@Composable
fun VersionSettings(modifier: Modifier = Modifier, title: String, appVersion: String) {
    Row(
        modifier = modifier
            .padding(16.dp)
            /* improving screen reader readability, with this the 2 Text are combined */
            .semantics(mergeDescendants = true) {},
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(text = title, modifier = Modifier.weight(1f))

        Text(text = appVersion)
    }
}