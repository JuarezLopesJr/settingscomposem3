@file:OptIn(
    ExperimentalLifecycleComposeApi::class, ExperimentalMaterial3Api::class,
    ExperimentalLifecycleComposeApi::class
)

package com.example.settingscomposem3.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.settingscomposem3.BuildConfig
import com.example.settingscomposem3.R
import com.example.settingscomposem3.data.SettingsState
import com.example.settingscomposem3.utils.SectionSpacer
import com.example.settingscomposem3.viewmodel.SettingsViewModel

@Composable
fun SettingsScreen() {
    val settingsViewModel = viewModel<SettingsViewModel>()
    val state by settingsViewModel.uiState.collectAsStateWithLifecycle()

    SettingsList(state = state, settingsViewModel = settingsViewModel)
}

@Composable
fun SettingsList(
    modifier: Modifier = Modifier,
    state: SettingsState,
    settingsViewModel: SettingsViewModel
) {
    val scrollState = rememberScrollState()

    Scaffold(
        topBar = {
            SmallTopAppBar(
                title = {
                    Text(text = stringResource(R.string.settings_screen_title), maxLines = 1)
                },
                navigationIcon = {
                    IconButton(onClick = { }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = stringResource(
                                R.string.navigation_icon_description
                            )
                        )
                    }
                },
                scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
            )
        },
        content = { innerPadding ->
            Column(
                modifier = modifier
                    .fillMaxSize()
                    .verticalScroll(scrollState)
                    .padding(paddingValues = innerPadding)
            ) {
                NotificationSettings(
                    title = stringResource(R.string.enable_notifications),
                    checked = state.notificationsEnabled,
                    onCheckedChange = {
                        settingsViewModel.toggleNotificationSettings()
                    }
                )

                Divider()

                HintSettings(
                    title = stringResource(id = R.string.show_hints),
                    checked = state.hintsEnabled,
                    onShowHintToggled = {
                        settingsViewModel.toggleHintSettings()
                    }
                )

                Divider()

                SubscriptionSettings(
                    title = stringResource(id = R.string.manage_subscription),
                    description = stringResource(id = R.string.subscription_description),
                    icon = Icons.Default.ArrowForward,
                    onSubscriptionClicked = {}
                )

                Divider()

                SectionSpacer()

                MarketingSettings(
                    selectedOption = state.marketingOption,
                    onOptionSelected = settingsViewModel::setMarketingOption
                )

                Divider()

                ThemeSettings(
                    selectedTheme = state.themeOption,
                    onThemeSelected = settingsViewModel::setTheme
                )

                SectionSpacer()

                VersionSettings(
                    title = stringResource(id = R.string.settings_app_version_title),
                    appVersion = BuildConfig.VERSION_NAME
                )
            }
        }
    )
}