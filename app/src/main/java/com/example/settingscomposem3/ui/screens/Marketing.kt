package com.example.settingscomposem3.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.stateDescription
import androidx.compose.ui.unit.dp
import com.example.settingscomposem3.R
import com.example.settingscomposem3.data.MarketingOption
import com.example.settingscomposem3.utils.Tags.TAG_MARKETING_OPTION

@Composable
fun MarketingSettings(
    modifier: Modifier = Modifier,
    selectedOption: MarketingOption,
    onOptionSelected: (MarketingOption) -> Unit
) {
    val radioOptions =
        stringArrayResource(id = R.array.setting_options_marketing_choice)

    Column(
        modifier = modifier
            /* list the number of options to Talkback MUST BE PROVIDED */
            .selectableGroup()
            .padding(16.dp)
    ) {
        Text(text = stringResource(id = R.string.marketing_option))

        Spacer(modifier = Modifier.height(8.dp))

        radioOptions.forEachIndexed { index, option ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    /* + index to dynamic pass the radio item to tests */
                    .testTag(TAG_MARKETING_OPTION + index)
                    .selectable(
                        selected = selectedOption.id == index,
                        role = Role.RadioButton
                    ) {
                        val marketingOption = if (index == MarketingOption.ALLOWED.id) {
                            MarketingOption.ALLOWED
                        } else {
                            MarketingOption.NOT_ALLOWED
                        }

                        onOptionSelected(marketingOption)
                    }
                    .padding(12.dp),
                verticalAlignment = CenterVertically
            ) {
                RadioButton(
                    selected = selectedOption.id == index,
                    onClick = null
                )

                Text(text = option, modifier = Modifier.padding(start = 8.dp))
            }
        }
    }
}