package com.example.settingscomposem3.ui.screens

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import com.example.settingscomposem3.utils.SettingsClickableItem

@Composable
fun SubscriptionSettings(
    modifier: Modifier = Modifier,
    title: String,
    description: String,
    icon: ImageVector,
    onSubscriptionClicked: () -> Unit
) {
    SettingsClickableItem(
        modifier = modifier,
        title = title,
        description = description,
        icon = icon,
        onClicked = onSubscriptionClicked
    )
}