package com.example.settingscomposem3.ui.screens

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import com.example.settingscomposem3.R
import com.example.settingscomposem3.utils.SettingsToggleableItem
import com.example.settingscomposem3.utils.Tags.TAG_TOGGLE_ITEM

@Composable
fun NotificationSettings(
    modifier: Modifier = Modifier,
    title: String,
    checked: Boolean,
    onCheckedChange: (Boolean) -> Unit
) {
    val notificationsState = if (checked) {
        stringResource(R.string.notification_enabled_text)
    } else {
        stringResource(R.string.notification_disabled_text)
    }

    SettingsToggleableItem(
        modifier = modifier,
        title = title,
        checked = checked,
        role = Role.Switch,
        semanticValue = notificationsState,
        tag = TAG_TOGGLE_ITEM,
        onToggled = onCheckedChange
    )
}