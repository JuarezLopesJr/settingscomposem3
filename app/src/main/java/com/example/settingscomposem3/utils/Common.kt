package com.example.settingscomposem3.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.toggleable
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.stateDescription
import androidx.compose.ui.unit.dp

@Composable
fun SettingsToggleableItem(
    modifier: Modifier = Modifier,
    title: String,
    checked: Boolean,
    role: Role,
    semanticValue: String,
    tag: String = "",
    onToggled: (Boolean) -> Unit,
) {/* propagating the toggleable action to the entire Row, this will improve accessibility
    causing less friction when using a screen reader (Talkback) */
    Row(
        modifier = modifier
            .heightIn(min = 56.dp)
            /* tag (semantic property, not visible in the UI, used by testing and accessibility)
            to reference the UI element in tests */
            .testTag(tag)
            .toggleable(
                value = checked,
                onValueChange = onToggled,
                role = role
            )
            /* improving accessibility context, instead of read on/off it'll read
             the current semantic value */
            .semantics {
                stateDescription = semanticValue
            }
            .padding(horizontal = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(text = title, modifier = Modifier.weight(1f))

        when (role) {
            Role.Switch -> Switch(checked = checked, onCheckedChange = null)
            Role.Checkbox -> Checkbox(checked = checked, onCheckedChange = null)
        }
    }
}

@Composable
fun SettingsClickableItem(
    modifier: Modifier = Modifier,
    title: String,
    description: String,
    icon: ImageVector,
    onClicked: () -> Unit
) {
    Row(modifier = modifier
        .heightIn(min = 56.dp)
        .clickable(onClickLabel = description) { onClicked() }
        .padding(horizontal = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(text = title, modifier = Modifier.weight(1f))
        Icon(imageVector = icon, contentDescription = null)
    }
}

@Composable
fun SectionSpacer(modifier: Modifier = Modifier) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .height(48.dp)
            .background(MaterialTheme.colorScheme.onSurface.copy(alpha = 0.12f))
    )
}

object Tags {
    const val TAG_TOGGLE_ITEM = "toggle_item"
    const val TAG_CHECK_ITEM = "check_item"

    /* _ to reference every option in the RadioGroup */
    const val TAG_MARKETING_OPTION = "marketing_item_"
    const val TAG_THEME_ITEM = "theme_item"
    const val TAG_SELECT_THEME = "select_theme"
    const val TAG_THEME_OPTION = "theme_"
    const val TAG_VERSION_ITEM = "version_item"
}