package com.example.settingscomposem3.viewmodel

import androidx.lifecycle.ViewModel
import com.example.settingscomposem3.data.MarketingOption
import com.example.settingscomposem3.data.SettingsState
import com.example.settingscomposem3.data.Theme
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class SettingsViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(SettingsState())
    val uiState = _uiState.asStateFlow()

    fun toggleNotificationSettings() {
        _uiState.update {
            it.copy(notificationsEnabled = !_uiState.value.notificationsEnabled)
        }
    }

    fun toggleHintSettings() {
        _uiState.update {
            it.copy(hintsEnabled = !_uiState.value.hintsEnabled)
        }
    }

    fun setMarketingOption(option: MarketingOption) {
        _uiState.update {
            it.copy(marketingOption = option)
        }
    }

    fun setTheme(theme: Theme) {
        _uiState.update {
            it.copy(themeOption = theme)
        }
    }
}