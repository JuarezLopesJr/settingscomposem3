package com.example.settingscomposem3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.settingscomposem3.data.Theme
import com.example.settingscomposem3.ui.screens.ThemeSettings
import com.example.settingscomposem3.utils.Tags.TAG_SELECT_THEME
import com.example.settingscomposem3.utils.Tags.TAG_THEME_ITEM
import com.example.settingscomposem3.utils.Tags.TAG_THEME_OPTION
import org.junit.Rule
import org.junit.Test

class ThemeSettingItemTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Selected_Theme_Displayed() {
        val option = Theme.DARK

        composeTestRule.setContent {
            ThemeSettings(selectedTheme = option, onThemeSelected = {})
        }

        composeTestRule.onNodeWithTag(TAG_THEME_ITEM, useUnmergedTree = true)
            .assertTextEquals(
                InstrumentationRegistry.getInstrumentation().targetContext
                    .getString(option.label)
            )
    }

    @Test
    fun assert_Theme_Options_Displayed() {
        composeTestRule.setContent {
            ThemeSettings(selectedTheme = Theme.DARK, onThemeSelected = {})
        }

        composeTestRule.onNodeWithTag(TAG_SELECT_THEME).performClick()

        Theme.values().forEach { theme ->
            composeTestRule
                .onNodeWithTag(
                    TAG_THEME_OPTION + InstrumentationRegistry
                        .getInstrumentation()
                        .targetContext
                        .getString(theme.label)
                ).assertIsDisplayed()
        }
    }
}