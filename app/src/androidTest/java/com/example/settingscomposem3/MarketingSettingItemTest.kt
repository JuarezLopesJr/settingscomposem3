package com.example.settingscomposem3

import androidx.compose.ui.test.assertIsSelected
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import com.example.settingscomposem3.data.MarketingOption
import com.example.settingscomposem3.ui.screens.MarketingSettings
import com.example.settingscomposem3.utils.Tags.TAG_MARKETING_OPTION
import org.junit.Rule
import org.junit.Test

class MarketingSettingItemTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Marketing_Option_Selected() {
        val option = MarketingOption.NOT_ALLOWED

        composeTestRule.setContent {
            MarketingSettings(selectedOption = option, onOptionSelected = {})
        }

        composeTestRule.onNodeWithTag(TAG_MARKETING_OPTION + option.id)
            .assertIsSelected()
    }
}