package com.example.settingscomposem3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import com.example.settingscomposem3.ui.screens.VersionSettings
import com.example.settingscomposem3.utils.Tags.TAG_VERSION_ITEM
import org.junit.Rule
import org.junit.Test

class AppVersionSettingItemTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_App_Version_Displayed() {
        val appVersion = "1.0"

        composeTestRule.setContent {
            VersionSettings(title = "", appVersion = appVersion)
        }

        composeTestRule.onNodeWithText(appVersion).assertIsDisplayed()
    }
}