package com.example.settingscomposem3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsOn
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import com.example.settingscomposem3.ui.screens.NotificationSettings
import com.example.settingscomposem3.utils.Tags.TAG_TOGGLE_ITEM
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class NotificationSettingItemTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setNotificationComposable() {
        composeTestRule.setContent {
            NotificationSettings(
                title = "enable notification",
                checked = true,
                onCheckedChange = {}
            )
        }
    }

    @Test
    fun assert_Title_Displayed() {
        composeTestRule.onNodeWithText("enable notification").assertIsDisplayed()
    }

    @Test
    fun assert_Setting_Checked() {
        composeTestRule.onNodeWithTag(TAG_TOGGLE_ITEM).assertIsOn()
    }
}