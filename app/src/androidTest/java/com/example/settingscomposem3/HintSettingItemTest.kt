package com.example.settingscomposem3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsOn
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import com.example.settingscomposem3.ui.screens.HintSettings
import com.example.settingscomposem3.utils.Tags.TAG_CHECK_ITEM
import org.junit.Before
import org.junit.Rule
import org.junit.Test

const val HINT_TITLE = "show hints"

class HintSettingItemTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setHintComposable() {
        composeTestRule.setContent {
            HintSettings(
                title = HINT_TITLE,
                checked = true,
                onShowHintToggled = {}
            )
        }
    }

    @Test
    fun assert_Text_Title_Displayed() {
        composeTestRule.onNodeWithText(HINT_TITLE).assertIsDisplayed()
    }

    @Test
    fun assert_Setting_Checked() {
        composeTestRule.onNodeWithTag(TAG_CHECK_ITEM).assertIsOn()
    }
}