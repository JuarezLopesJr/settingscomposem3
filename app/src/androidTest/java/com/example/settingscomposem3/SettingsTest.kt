package com.example.settingscomposem3

import androidx.annotation.StringRes
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsOn
import androidx.compose.ui.test.assertIsSelected
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.settingscomposem3.ui.screens.SettingsScreen
import com.example.settingscomposem3.utils.Tags.TAG_CHECK_ITEM
import com.example.settingscomposem3.utils.Tags.TAG_MARKETING_OPTION
import com.example.settingscomposem3.utils.Tags.TAG_TOGGLE_ITEM
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SettingsTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent {
            SettingsScreen()
        }
    }

    private fun assertSettingIsDisplayed(@StringRes title: Int) {
        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(title)
        ).assertIsDisplayed()
    }

    private fun getNodeWithTextSemanticsAndPerformClick(@StringRes title: Int) {
        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(title)
        ).performClick()
    }


    @Test
    fun assert_Enable_Notifications_Settings_Is_Displayed() {
        assertSettingIsDisplayed(R.string.enable_notifications)
    }

    @Test
    fun assert_Hint_Settings_Is_Displayed() {
        assertSettingIsDisplayed(R.string.show_hints)
    }

    @Test
    fun assert_View_Subscription_Settings_Is_Displayed() {
        assertSettingIsDisplayed(R.string.manage_subscription)
    }

    @Test
    fun assert_Marketing_Settings_Is_Displayed() {
        assertSettingIsDisplayed(R.string.marketing_option)
    }

    @Test
    fun assert_Theme_Settings_Is_Displayed() {
        assertSettingIsDisplayed(R.string.theme_option)
    }

    @Test
    fun assert_App_Version_Settings_Is_Displayed() {
        assertSettingIsDisplayed(R.string.settings_app_version_title)
    }

    @Test
    fun assert_Enable_Notifications_Toggles_Selected_State() {
        getNodeWithTextSemanticsAndPerformClick(R.string.enable_notifications)
        composeTestRule.onNodeWithTag(TAG_TOGGLE_ITEM).assertIsOn()
    }

    @Test
    fun assert_Enable_Hints_Checks_Selected_State() {
        getNodeWithTextSemanticsAndPerformClick(R.string.show_hints)
        composeTestRule.onNodeWithTag(TAG_CHECK_ITEM).assertIsOn()
    }

    /* TAG +1 means that the test will be applied to the next radio button item
     since by default the first one is already selected */
    @Test
    fun assert_Marketing_Options_Toggles_Selected_State() {
        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation().targetContext
                .resources.getStringArray(R.array.setting_options_marketing_choice)[1]
        ).performClick()
        
        composeTestRule.onNodeWithTag(TAG_MARKETING_OPTION + 1).assertIsSelected()
    }
}