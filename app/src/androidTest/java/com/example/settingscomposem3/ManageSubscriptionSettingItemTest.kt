package com.example.settingscomposem3

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import com.example.settingscomposem3.ui.screens.SubscriptionSettings
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

const val SUBSCRIPTION_TITLE = "manage subscription"

class ManageSubscriptionSettingItemTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Text_Title_Displayed() {
        composeTestRule.setContent {
            SubscriptionSettings(
                title = SUBSCRIPTION_TITLE,
                description = "",
                icon = Icons.Default.ArrowForward,
                onSubscriptionClicked = {}
            )
        }

        composeTestRule.onNodeWithText(SUBSCRIPTION_TITLE).assertIsDisplayed()
    }

    @Test
    fun assert_On_Setting_Clicked_Triggered() {
        val onClicked: () -> Unit = mock()

        composeTestRule.setContent {
            SubscriptionSettings(
                title = SUBSCRIPTION_TITLE,
                description = "",
                icon = Icons.Default.ArrowForward,
                onSubscriptionClicked = onClicked
            )
        }

        composeTestRule.onNodeWithText(SUBSCRIPTION_TITLE).performClick()

        verify(onClicked).invoke()
    }
}